#!/usr/bin/env sh

ssh -Yf $1 Xephyr  -br -query localhost -fullscreen \
 -verbosity 10  -keybd ephyr,,,xkbrules=evdev,xkbmodel=evdev,xkblayout=es :1 
