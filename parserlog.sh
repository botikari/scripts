#!/usr/bin/env sh
#
# _autor_= """Juan Aguilar (jaguilar@ub.edu)"""
# Description = """Contador de ocurrencias en diferentes ficheros ,
# entrada en maquina, recogida de logs y muestreo de estos"""
# Date = """mar sep 23 14:33:26 CEST 2014"""
# rev = 0.1

#usage#{{{
usage=$(
cat <<EOF
$0	USER@HOST PATTERN FILES DIR SERIES STARTDATE ENDDATE [REGEXDATE]
	
[USER] Usuario del que recuperar logs. 
	[HOST] Host del que recuperar logs.
	[PATTERN] Regex que buscar en los logs.
	[FILES] Espression de los ficheros a parsear, se pueden introducir las
	siguientes variables en ellos #date# y #series#. 
	Por ejemplo :
	acces-c#date#-#series# ara una combinatoria donde #date# sera sustituido
	por el margen de fechas y series por un margen de numeros.
	[DIR] Directorio raiz donde se encuentra los ficheros tambien se le
	pueden introducir las dos variables.
	[SERIES] Serie numerica separa mediante espacios, se recomienda utilizar
	\`seq i f\`, o si son del modo 01 02 03, etc \`seq -w i f\` mantendra los 
	zeros a la derecha.
	[STARTDATE] Fecha de inicio del tipo  YYYYmmdd. Por ejemplo el 9 de
	Septiembre de 2014: 20140923. 
	[ENDDATE] Fecha de fin  del tipo YYYYmmdd. Por ejemplo el 9 de
	23 Septiembre de 2014: 20140923.
	[REGEXDATE] OPT. s per servidors Solaris
					 l per servidors amb syslog (linux)
	Need coreutils, perl, sshpass and tools for extract.
EOF
)
#}}}

# Extract Files#{{{
extract() 
{
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.tar.xz)    tar xvJf $1    ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       unrar x $1     ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *.xz)        unxz $1        ;;
          *.exe)       cabextract $1  ;;
          *)           printf "%s: unrecognized file compression\n" $1\
						&& exit 1
						;;
      esac
  else
      printf "%s is not a valid file" $1 && exit 1
  fi
}
#}}}

# Parser#{{{
parser()
{
	o=`egrep $2`
	case $1 in
		s)		return `echo $o\
			| egrep -o "\[[0-3][0-9]\/[A-Z][a-z]+\/201[4-9]:.*\] "\
			| cut -c2-18` 
			;;
		l)		return `echo $o\ 
			| perl -n -e 'use DateTime::Format::Strptime; my $parser =
			DateTime::Format::Strptime->new( pattern => "%B %d %Y"); m/^(\w+
			\d+ )(.*)/; print ($parser->parse_datetime("$1" .
			DateTime->now->year)->ymd, " ", $2, "\n");'`
			;;
		*)		printf "%s\n" $usage; exit 1; 
			;;
	esac
}
#}}}

# Countdays#{{{
countdays()
{
	return `awk -F, '{ OFS=","; print $1,$2,$3 }' $1 | uniq -c`
}
#}}}

# Counthours #{{{
counthours()
{
	return `awk -F, '{ OFS=","; print $1,$2,$3,$4 }' $1 | uniq -c`
}
#}}}

# Main#{{{
[ $# -le 7 ]  && echo $usage &&  exit 1
[ ! -x /usr/bin/sshpass ] &&  printf "Sshpass no esta instalat\n" && exit 1

foldate=`date --date="$6" +%Y%m%d`
[ $? -ne 0 ] && exit 1
currentdate=`date  --date="$7" +%Y%m%d`
[ $? -ne 0 ] && exit 1

## Carpeta temporal 
TMPDIR=`mktemp -d` 
cd $TMPDIR
printf "Directori de treball %s\n" $TMPDIR

## Loop per carpetes path+numero
printf "Introduiu el password del %s:\n" $1
read -s SSHPASS
for serie in $5; 
do
	foldate=`date --date="$6" +%Y%m%d`
	mkdir $serie
	cd $serie
	until [ "$foldate" = "$currentdate" ]
	do
		#Recuperacio de  arxius
		ofile=`echo $3 | sed s'/#date#/'"${foldate}"'/g'\
			| sed s'/#serie#/'"${serie}"'/'`
		odir=`echo $4 | sed s'/#date#/'"${foldate}"'/g'\
			| sed s'/#serie#/'"${serie}"'/'`

		#echo $1:$odir$ofile 
		sshpass -e scp $1:$odir$ofile . 

		extract $ofile && rm $ofile
		[ $? -ne 0 ] &&  printf "Fail to extract %s\n" $ofile && exit 1

		foldate=`date --date="$foldate 1 day" +%Y%m%d`

	done

	cd $TMPDIR

	[ -n $8 ] && machine="l" && printf "Default machine its Syslog linux\n" ||
		machine=$8

	for file in $serie/*:
	do
		parser $machine $2  $file >> flog$serie
	done
	rm -d $serie

	# To csv 
	fout=log$serie"at"$1"/"$2":"$3"to"$4.csv  
	echo DAY,MONTH,YEAR,HOUR,MINUTE >> $fout
	awk -F[/:] '{ OFS=","; print $0 }' < flog$serie >\
	$fout  && rm flog$series
	
	printf "Ocurencies de %s en %s serie %s del %s al %s acumulats per dies:\n%s"\ 
	$2 $1 $serie "`date --date=$7 '+%A %d de %B del %Y'`"\ 
	"`date --date=$7 '+%A %d de %B del %Y'`" "`countdays $fout`"

	printf "Ocurencies de %s en %s serie %s del %s al %s acumulats per hores:\n%s"\ 
	$2 $1 $serie "`date --date=$7 '+%A %d de %B del %Y'`"\ 
	"`date --date=$7 '+%A %d de %B del %Y'`" "`counthours $fout`"
done
unset SSHPASS

#}}}
